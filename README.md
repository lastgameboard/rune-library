# Rune Library


## Quick Guide

Welcome to the rune library. These are all the currently supported runes, more runes and base designs will be added.

Besides printing spares, you can also modify the bases to fit any minature.

**Do not modify the rune (the shape that contacts the Gameboard) the Gameboard will have trouble recognizing it, this includes the overall size**

## Officially Supported Materials

- [Conductive PLA from Protopasta](https://www.proto-pasta.com/products/conductive-pla)

Most conductive materials should work. As we test more, we will add to this list. 
**WARNING: Metal or harder plastics may scratch your screen.**
